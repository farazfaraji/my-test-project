import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Endpoints } from '@octokit/types';
import { Octokit } from 'octokit';
import { ObservedRepoType } from '../observed-repos/types/observed-repo.type';

export type GetGithubRepoResponse =
  Endpoints['GET /repos/{owner}/{repo}']['response']['data'];

@Injectable()
export class FetcherService {
  private readonly octokit;

  constructor(private readonly configService: ConfigService) {
    this.octokit = new Octokit({
      auth: this.configService.get('GITHUB_API_KEY'),
    });
  }

  async getGithubRepo(owner: string, name: string): Promise<ObservedRepoType> {
    try {
      console.log(`send request to github by ${owner}/${name}`);
      const result = await this.octokit.request(`GET /repos/${owner}/${name}`, {
        headers: {
          'X-GitHub-Api-Version': '2022-11-28',
        },
      });

      return {
        url: result.url,
        owner: result.owner,
        name: result.name,
        stars: result.stargazers_count || 0,
        openIssues: result.open_issues || 0,
        license: result.license?.name || '',
      };
    } catch (e) {
      throw new Error(e);
    }
  }
}
