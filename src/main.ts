import { INestApplication, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Repo')
    .setDescription('Repo descriptions')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api', app, document);

  const config = app.get(ConfigService);
  configureMicroservices(app, config);
  app.startAllMicroservices();

  await app.listen(8080);
}

async function configureMicroservices(
  app: INestApplication,
  config: ConfigService,
) {
  app.enableShutdownHooks();

  app.connectMicroservice({
    transport: Transport.RMQ,
    debugger: true,
    options: {
      queue: 'notification',
      urls: [config.get<string>('RMQ_URL')],
      prefetchCount: 1,
    },
  });
}
bootstrap();
