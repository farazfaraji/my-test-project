import { Process, Processor } from '@nestjs/bull';
import { ObservedRepo } from '@prisma/client';
import { Job } from 'bullmq';
import {
  FetcherService,
  GetGithubRepoResponse,
} from '../fetcher/fetcher.service';
import { UpdateObservedRepoDto } from '../observed-repos/dto/update-observed-repo.dto';
import { ObservedReposService } from '../observed-repos/service/observed-repos.service';
import { NotificationService } from './notification.service';
import { ObservedRepoType } from 'src/observed-repos/types/observed-repo.type';

@Processor('github-cron')
export class WorkerService {
  constructor(
    private readonly notificationService: NotificationService,
    private readonly fetcherService: FetcherService,
    private readonly observedReposService: ObservedReposService,
  ) {}

  @Process('repo-data')
  async processUserData(job: Job<ObservedRepo>) {
    console.log(`start processing on ${job.data.url}`);

    const data = await this.fetcherService.getGithubRepo(
      job.data.owner,
      job.data.name,
    );

    const isThereAnyChange = this.isThereAnyChange(data, job.data);

    if (isThereAnyChange) {
      const dataToUpdate: UpdateObservedRepoDto = {
        stars: data.stars,
        openIssues: data.openIssues,
        license: data.license,
      };
      await this.observedReposService.updateObservedRepo(
        job.data.id,
        dataToUpdate,
      );

      await this.notificationService.sendNotification(job.data);
    }
  }

  isThereAnyChange(githubData: ObservedRepoType, existData: ObservedRepo) {
    return (
      githubData.license === existData.license &&
      githubData.openIssues === existData.openIssues &&
      githubData.stars === existData.stars
    );
  }
}
