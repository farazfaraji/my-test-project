import { Logger, Module } from '@nestjs/common';
import { ObservedReposService } from 'src/observed-repos/service/observed-repos.service';
import { PrismaService } from 'src/prisma.service';
import { FetcherService } from '../fetcher/fetcher.service';
import { CronService } from './cron.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RedisModule } from '@liaoliaots/nestjs-redis';
import { BullModule } from '@nestjs/bull';
import { configValidationSchema } from 'src/config.schema';
import { WorkerService } from './worker.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { NotificationService } from './notification.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`${process.env.NODE_ENV || ''}.env`],
      validationSchema: configValidationSchema,
    }),
    RedisModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        config: {
          host: configService.get('REDIS_HOST'),
          port: configService.get<number>('REDIS_PORT'),
        },
      }),
    }),
    BullModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        redis: {
          host: configService.get('REDIS_HOST'),
          port: configService.get<number>('REDIS_PORT'),
        },
      }),
    }),
    BullModule.registerQueue({
      name: 'github-cron',
    }),
    ClientsModule.registerAsync([
      {
        name: 'NOTIFICATION-SERVICE',
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.RMQ,
          options: {
            queue: 'notification',
            urls: [configService.get<string>('RMQ_URL')],
            queueOptions: { durable: 1 },
          },
        }),
        inject: [ConfigService],
      },
    ]),
  ],
  controllers: [],
  providers: [
    NotificationService,
    CronService,
    PrismaService,
    Logger,
    FetcherService,
    ObservedReposService,
    WorkerService,
  ],
})
export class CronModule {}
