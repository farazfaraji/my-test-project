import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { ObservedRepo } from '@prisma/client';

@Injectable()
export class NotificationService {
  constructor(
    @Inject('NOTIFICATION-SERVICE') private readonly clientRMQ: ClientProxy,
  ) {}

  async sendNotification(data: ObservedRepo) {
    await this.clientRMQ.emit('notification.update', JSON.stringify(data));
  }
}
