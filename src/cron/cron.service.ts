import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Interval } from '@nestjs/schedule';
import { Queue } from 'bullmq';
import { ObservedReposService } from 'src/observed-repos/service/observed-repos.service';

@Injectable()
export class CronService {
  constructor(
    private readonly observedReposService: ObservedReposService,
    @InjectQueue('github-cron') private readonly queue: Queue,
  ) {}

  // if you want to make it dynamic, you can use repeatable job from the bullMQ
  @Interval('messaging', 300000)
  async addJob() {
    console.log('Start fetching from Github');

    const repos = await this.observedReposService.getAllActiveRepos();
    for (const repo of repos) {
      await this.queue.add('repo-data', repo);
    }
  }
}
