import { Controller } from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';
import { ObservedRepo } from '@prisma/client';

@Controller()
export class NotificationConsumer {
  @EventPattern('notification.update')
  async handleRepoUpdate(@Payload() data: ObservedRepo) {
    console.log(`Received message: ${data}`);
  }
}
