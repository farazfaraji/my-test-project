import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { ObservedReposController } from './controller/observed-repos.controller';
import { ObservedReposService } from './service/observed-repos.service';
import { FetcherService } from 'src/fetcher/fetcher.service';
import { ConfigModule } from '@nestjs/config';
import { configValidationSchema } from 'src/config.schema';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`${process.env.NODE_ENV || ''}.env`],
      validationSchema: configValidationSchema,
    }),
  ],
  controllers: [ObservedReposController],
  providers: [ObservedReposService, PrismaService, FetcherService],
  exports: [FetcherService],
})
export class ObservedReposModule {}
