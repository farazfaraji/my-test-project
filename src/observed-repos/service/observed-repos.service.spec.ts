import { Test, TestingModule } from '@nestjs/testing';
import { ObservedReposService } from './observed-repos.service';
import { PrismaService } from '../../prisma.service';
import { FetcherService } from '../../fetcher/fetcher.service';
import { CreateObservedRepoDto } from '../dto/create-observed-repo.dto';
import { ConfigService } from '@nestjs/config';
import {
  GetAllObservedReposDto,
  GetAllObservedReposResponse,
} from '../dto/get-all-obersved-repos.dto';
import { NotFoundException } from '@nestjs/common';

describe('ObservedReposService', () => {
  let service: ObservedReposService;
  let prismaService: PrismaService;

  const mockPrismaService = {
    observedRepo: {
      count: jest.fn(),
      create: jest.fn(),
      findMany: jest.fn(),
    },
  };

  const mockFetcherService = {
    getGithubRepo: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ConfigService,
        ObservedReposService,
        PrismaService,
        {
          provide: PrismaService,
          useValue: mockPrismaService,
        },
        {
          provide: FetcherService,
          useValue: mockFetcherService,
        },
      ],
    }).compile();

    service = module.get<ObservedReposService>(ObservedReposService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('createObservedRepo', () => {
    it('should create an observed repo', async () => {
      const createObservedRepoDto: CreateObservedRepoDto = {
        owner: 'testowner',
        name: 'testname',
      };

      mockPrismaService.observedRepo.count.mockResolvedValueOnce(0);
      mockFetcherService.getGithubRepo.mockResolvedValueOnce({
        url: 'test-url',
        owner: 'testowner',
        name: 'testname',
        stars: 10,
        openIssues: 5,
        license: 'test-license',
      });

      mockPrismaService.observedRepo.create.mockResolvedValueOnce({
        id: 'id',
        license: 'test-license',
        name: 'testname',
        openIssues: 5,
        owner: 'testowner',
        stars: 10,
        status: 'ACTIVE',
        updatedAt: new Date(),
        createdAt: new Date(),
        url: 'test-url',
      });

      const result = await service.createObservedRepo(createObservedRepoDto);

      expect(result).toEqual({
        id: expect.any(String),
        url: 'test-url',
        owner: 'testowner',
        name: 'testname',
        stars: 10,
        openIssues: 5,
        license: 'test-license',
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        status: expect.any(String),
      });
    });

    it('should throw an error if repo already exists', async () => {
      const createObservedRepoDto: CreateObservedRepoDto = {
        owner: 'testowner',
        name: 'testname',
      };

      mockPrismaService.observedRepo.count.mockResolvedValueOnce(1);
      await expect(
        service.createObservedRepo(createObservedRepoDto),
      ).rejects.toThrowError('Repo exist in the database.');
    });
  });

  describe('getAllObservedRepos', () => {
    const mockObservedRepo = {
      id: '1',
      url: 'URL',
      name: 'Mock Repo',
      owner: 'Mock Owner',
      stars: 10,
      openIssues: 5,
      status: 'ACTIVE',
      license: 'MIT',
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    const mockDto: GetAllObservedReposDto = {};

    it('should return an array of observed repos', async () => {
      jest;
      mockPrismaService.observedRepo.findMany.mockResolvedValueOnce([
        mockObservedRepo,
      ]);

      mockPrismaService.observedRepo.count.mockResolvedValueOnce(20);

      const result: GetAllObservedReposResponse =
        await service.getAllObservedRepos(mockDto);

      expect(result).toEqual({
        nextPageToken: null,
        previousPageToken: null,
        totalPages: 1,
        page: 1,
        data: [mockObservedRepo],
      });
    });

    it('should throw NotFoundException if no data is found', async () => {
      mockPrismaService.observedRepo.findMany.mockResolvedValueOnce([]);

      await expect(service.getAllObservedRepos(mockDto)).rejects.toThrowError(
        NotFoundException,
      );
    });
  });
});
