import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ObservedRepo } from '@prisma/client';
import { PrismaService } from '../../prisma.service';
import { CreateObservedRepoDto } from '../dto/create-observed-repo.dto';
import {
  GetAllObservedReposDto,
  GetAllObservedReposResponse,
} from '../dto/get-all-obersved-repos.dto';
import { UpdateObservedRepoDto } from '../dto/update-observed-repo.dto';
import { FetcherService } from '../../fetcher/fetcher.service';
import { ConfigService } from '@nestjs/config';
import { Status } from '../enums/status.enum';

@Injectable()
export class ObservedReposService {
  constructor(
    private readonly configService: ConfigService,
    private readonly prisma: PrismaService,
    private readonly fetcherService: FetcherService,
  ) {}

  async getAllObservedRepos(
    query: GetAllObservedReposDto,
  ): Promise<GetAllObservedReposResponse> {
    const { pageToken } = query;

    let skip = 0;
    let page = 1;

    const pageSize = parseInt(this.configService.get('PER_PAGE_ITEM'), 10);

    if (pageToken) {
      page = parseInt(Buffer.from(pageToken, 'base64').toString());
      skip = page * pageSize;
    }

    const [repos, totalCount] = await Promise.all([
      this.prisma.observedRepo.findMany({
        select: {
          id: true,
          name: true,
          owner: true,
          status: true,
          license: true,
        },
        skip,
        take: pageSize,
      }),
      this.prisma.observedRepo.count(),
    ]);

    if (!repos.length) {
      throw new NotFoundException('There is no data to fetch');
    }

    const totalPages = Math.ceil(totalCount / pageSize);

    const nextPage = page < totalPages ? page + 1 : null;
    const previousPage = page > 0 ? page - 1 : null;

    const nextPageToken = nextPage
      ? Buffer.from(nextPage.toString()).toString('base64')
      : null;
    const previousPageToken = previousPage
      ? Buffer.from(previousPage.toString()).toString('base64')
      : null;

    return {
      nextPageToken,
      previousPageToken,
      page,
      totalPages,
      data: repos,
    };
  }

  async getObservedRepo(id: string): Promise<ObservedRepo | null> {
    const repo = await this.prisma.observedRepo.findFirst({
      where: { id },
    });

    if (!repo) {
      throw new NotFoundException(`Repo with ID ${id} not found.`);
    }

    return repo;
  }

  async createObservedRepo(
    createObservedRepoDto: CreateObservedRepoDto,
  ): Promise<ObservedRepo> {
    const { owner, name } = createObservedRepoDto;

    const isRepoExist = await this.isRepoExist(owner, name);
    if (isRepoExist) {
      throw new BadRequestException('Repo exist in the database.');
    }

    const data = await this.fetcherService.getGithubRepo(owner, name);

    return this.prisma.observedRepo.create({
      data,
    });
  }

  async updateObservedRepo(
    id: string,
    body: UpdateObservedRepoDto,
  ): Promise<ObservedRepo> {
    return this.prisma.observedRepo.update({
      where: { id },
      data: body,
    });
  }

  async deleteObservedRepo(id: string): Promise<ObservedRepo> {
    return this.prisma.observedRepo.delete({
      where: { id },
    });
  }

  async getAllActiveRepos(): Promise<ObservedRepo[]> {
    return await this.prisma.observedRepo.findMany({
      where: { status: Status.ACTIVE },
    });
  }

  async isRepoExist(owner: string, name: string): Promise<boolean> {
    return (
      (await this.prisma.observedRepo.count({ where: { owner, name } })) > 0
    );
  }
}
