export type ObservedRepoType = {
  url: string;
  owner: string;
  name: string;
  stars: number;
  openIssues: number;
  license: string;
};
