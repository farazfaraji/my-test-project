import { ApiProperty } from '@nestjs/swagger';
import { Status } from '../enums/status.enum';
import { IsEnum, IsOptional } from 'class-validator';

export class UpdateObservedRepoDto {
  @ApiProperty({ enum: Status })
  @IsOptional()
  @IsEnum(Status)
  status?: Status;

  @ApiProperty({ type: Number })
  @IsOptional()
  stars?: number;

  @ApiProperty({ type: Number })
  @IsOptional()
  openIssues?: number;

  @ApiProperty({ type: String })
  @IsOptional()
  license?: string;
}
