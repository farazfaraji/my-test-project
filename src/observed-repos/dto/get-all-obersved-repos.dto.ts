import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class GetAllObservedReposDto {
  @ApiProperty({ type: String })
  @IsString()
  @IsOptional()
  pageToken?: string;
}

export class ObservedReposResponse {
  @ApiProperty({ type: String })
  id: string;

  @ApiProperty({ type: String })
  status: string;

  @ApiProperty({ type: String })
  owner: string;

  @ApiProperty({ type: String })
  name: string;

  @ApiProperty({ type: String })
  license: string;
}

export class GetAllObservedReposResponse {
  @ApiProperty({ type: String })
  nextPageToken: string;

  @ApiProperty({ type: String })
  previousPageToken: string;

  @ApiProperty({ type: Number })
  totalPages: number;

  @ApiProperty({ type: Number })
  page: number;

  @ApiProperty({ isArray: true, type: ObservedReposResponse })
  data: ObservedReposResponse[];
}
